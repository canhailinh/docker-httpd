FROM httpd:latest

CP ./httpd.conf /usr/local/apache2/conf/httpd.conf
CP ./index.html /usr/local/apache2/htdocs

EXPORT 80
CMD [“/usr/sbin/httpd”,” -D”,” FOREGROUND”]
